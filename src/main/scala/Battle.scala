import akka.actor.ActorLogging
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Actor.Receive
import akka.actor.Props
import akka.actor.ActorSystem
import scala.concurrent.duration.*
val random = scala.util.Random

case object Fire
case object Init

class HigherPower extends Actor {
  def receive: Receive = {
    case Fire => 
      val castle = context.actorSelection("/user/CastleOf*")
      castle ! Fire
  }
}

object Castle {
  case class Attack(unitSize: Int)
}
class Castle(kingdom: String) extends Actor with ActorLogging {
  import Castle._

  def receive: Receive = {
    case Init => {
      for {
        i <- (1 to 100).toList
      } yield {
        val defender = context.actorOf(Props(Defender(kingdom)), s"DefenderOf$kingdom$i")
        defender ! Init
      }
      context.become(battle(100))
    }
  }
  
  def battle(unitSize: Int): Receive = {
    case Fire =>
      val army = context.actorSelection(s"/user/CastleOf$kingdom/DefenderOf$kingdom*")

      log.info(s"$kingdom starts fire with $unitSize knights!")

      army ! Attack(unitSize)

    case Defender.Shot(enemy) => 
      if (enemy != kingdom) {
        val thisKnight = random.nextInt(100) + 1 
        val gotShot = context.actorSelection(s"/user/CastleOf$enemy/DefenderOf$enemy$thisKnight")
        gotShot ! Defender.Shot(enemy)
      }
    case Defender.Dead =>
      // if a number > 600 is drawn, the army regroups.
      val regroupingChance = random.nextInt(1000)
      
      if (unitSize == 1) {
        if (regroupingChance > 600) {
          log.info("Next unit starts the battle!")
          self ! Init
        } else {
          log.info("Hundres Years' War is over!")
          log.info(s"$kingdom loses!")
          context.become(battle(unitSize - 1))
          context.system.terminate()
        }
      } else {
        context.become(battle(unitSize - 1))
      }
  }
}

object Defender {
  case class Shot(kingdom: String)
  case object Dead
}
class Defender(of: String) extends Actor with ActorLogging {
  import Defender._

  def receive: Receive = {
    case Init => context.become(active)
  }

  def active: Receive = {
    case Castle.Attack(size) => 
      val castles = context.actorSelection("/user/CastleOf*")
      castles ! Attack(of)
    
    case Shot(army) =>
      // if a number > 50 is drawn, the knight dies. otherwise they still fight.
      val survivingChances = random.nextInt(100)
      if (survivingChances > 50) {
        context.parent ! Dead
        context.become(dead)
      }
  }

  def dead: Receive = {
    case _ =>
  }
}

@main def hundredYearsBattle: Unit = {
  val system = ActorSystem("HundredYearsBattle")
  implicit val executionContext = system.dispatcher

  val higherPower = system.actorOf(Props[HigherPower](), "HigherPower")
  val englishCastle = system.actorOf(Props(Castle("England")), "CastleOfEngland")
  val frenchCastle = system.actorOf(Props(Castle("France")), "CastleOfFrance")
  val spanishCastle = system.actorOf(Props(Castle("Spain")), "CastleOfSpain")

  val config = system.settings.config
  val delay = config.getInt("scheduler.delay").milli

  system.scheduler.scheduleWithFixedDelay(
    delay,
    delay,
    higherPower,
    Fire
  )

  englishCastle ! Init
  frenchCastle ! Init
  spanishCastle ! Init
}