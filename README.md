# Battle of castles

This is a simple project created with Akka framework in Scala. It simulates a battle between two or more sides using the actor system and scheduler. `sbt` is needed to run the project.

### Tech stack

`Scala 3` `Akka`

### Description

The Higher Power gives an attack command to all factions engaged in the battle every 5 seconds. Each castle begins with a garrison of 100 knights.

During combat, there is a 50% chance that a knight will perish. If all defenders are eliminated, there is a 40% chance that the castle will regroup and restart with 100 knights.

The ultimate victor on the battlefield is the last standing castle.
